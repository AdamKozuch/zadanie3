##Część pierwsza - Przeprowadzanie MapReduce na pliku anagramy
Po wczytaniu do bazy pliku word_list.txt do bazy 
funkcja map wygląda następująco 

```javascript
function() {
var word=  this.word.split("");
word =  word.sort();
word=  word.join();
  emit(word,this.word);
}
```
Natomiast funkcja reduce ma taką postać :
```javascript
function(key, values) {
  return values.toString();
};
```

![](https://cloud.githubusercontent.com/assets/5136443/5631485/647e413e-95c7-11e4-8f03-e635f9fb534a.png)

Jak widzimy mapReduce redukuje 914 slów co jest równoznaczne z otrzymaniem 914 słów które są anagramami innych słów.

aby zobaczyć wszystkie anagramy możemy użyc funkcji
```javascript
db.anag.out.find({$where:"this.value.length>6"})
```
Każda wartość która ma więcej niż 6 znaków zawiera więcej niż jedno słowo i jest anagramem.